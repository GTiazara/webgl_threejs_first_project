
import * as THREE from 'three'

// NOTE: three/addons alias or importmap does not seem to be supported by Parcel, use three/examples/jsm/ instead 

import {
  OrbitControls
} from 'three/examples/jsm/controls/OrbitControls.js';

import {
  GLTFLoader
} from 'three/examples/jsm/loaders/GLTFLoader.js';

import * as SkeletonUtils from 'three/examples/jsm/utils/SkeletonUtils.js';


import { Octree } from 'three/examples/jsm/math/Octree.js';
import { OctreeHelper } from 'three/examples/jsm/helpers/OctreeHelper.js';

import { Capsule } from 'three/examples/jsm/math/Capsule.js';
import { commandOptions } from 'redis';

import { GUI } from 'three/examples/jsm/libs/lil-gui.module.min.js';
// Example of hard link to official repo for data, if needed
// const MODEL_PATH = 'https://raw.githubusercontent.com/mrdoob/three.js/r148/examples/models/gltf/LeePerrySmith/LeePerrySmith.glb';

function main() {
  // // INSERT CODE HERE
  const clock = new THREE.Clock();
  const scene = new THREE.Scene();
  const aspect = window.innerWidth / window.innerHeight;


  const camera = new THREE.PerspectiveCamera(70, window.innerWidth / window.innerHeight, 0.1, 1000);
  camera.rotation.order = 'YXZ';

  const fillLight1 = new THREE.HemisphereLight(0x4488bb, 0x002244, 0.5);
  fillLight1.position.set(2, 1, 1);
  scene.add(fillLight1);


  const directionalLight = new THREE.DirectionalLight(0xffffff, 0.8);
  directionalLight.position.set(- 5, 25, - 1);
  directionalLight.castShadow = true;
  directionalLight.shadow.camera.near = 0.01;
  directionalLight.shadow.camera.far = 500;
  directionalLight.shadow.camera.right = 30;
  directionalLight.shadow.camera.left = - 30;
  directionalLight.shadow.camera.top = 30;
  directionalLight.shadow.camera.bottom = - 30;
  directionalLight.shadow.mapSize.width = 1024;
  directionalLight.shadow.mapSize.height = 1024;
  directionalLight.shadow.radius = 4;
  directionalLight.shadow.bias = - 0.00006;
  scene.add(directionalLight);



  const renderer = new THREE.WebGLRenderer({ antialias: true });
  renderer.setPixelRatio(window.devicePixelRatio);
  renderer.setSize(window.innerWidth, window.innerHeight);
  renderer.shadowMap.enabled = true;
  renderer.shadowMap.type = THREE.VSMShadowMap;
  renderer.outputEncoding = THREE.sRGBEncoding;
  renderer.toneMapping = THREE.ACESFilmicToneMapping;
  document.body.appendChild(renderer.domElement);

  const controlsOrb = new OrbitControls(camera, renderer.domElement);
  controlsOrb.listenToKeyEvents(window); // optional

  const NUM_SPHERES = 15;
  const SPHERE_RADIUS = 2;

  const sphereGeometry = new THREE.IcosahedronGeometry(SPHERE_RADIUS, 5);
  const sphereMaterial = new THREE.MeshLambertMaterial({ color: 0xbbbb44 });

  const spheres = [];
  let sphereIdx = 0;

  for (let i = 0; i < NUM_SPHERES; i++) {

    const sphere = new THREE.Mesh(sphereGeometry, sphereMaterial);
    sphere.castShadow = true;
    sphere.receiveShadow = true;
    sphere.position.y = 2;
    sphere.position.x = - 30 + (Math.random() * 60);
    sphere.position.z = - 30 + (Math.random() * 60);

    scene.add(sphere);

    spheres.push({
      mesh: sphere,
      collider: new THREE.Sphere(new THREE.Vector3(0, - 100, 0), SPHERE_RADIUS),
      velocity: new THREE.Vector3()
    });

  }

  const manager = new THREE.LoadingManager(
    () => {
      console.log('loaded')
    },

    // Progress
    () => {
      console.log('progress')
    }
  );

  manager.onLoad = init

  let models = {
    player: { url: 'assets/models/Soldier.glb', name: 'player' },
    terrain: { url: 'assets/models/inazuma_eleven_terrain_commerce.glb', name: 'terrain' }
  };

  {
    const gltfLoader = new GLTFLoader(manager);
    for (const model of Object.values(models)) {
      gltfLoader.load(model.url, (gltf) => {
        model.gltf = gltf;
        scene.add(model.gltf.scene)
        if (model.name == "terrain") {

          worldOctree.fromGraphNode(gltf.scene);

          gltf.scene.traverse(child => {

            if (child.isMesh) {

              child.castShadow = true;
              child.receiveShadow = true;

              if (child.material.map) {

                child.material.map.anisotropy = 4;

              }

            }

          });

          const helper = new OctreeHelper(worldOctree);
          helper.visible = false;
          scene.add(helper);

          const gui = new GUI({ width: 200 });
          gui.add({ debug: false }, 'debug')
            .onChange(function (value) {

              helper.visible = value;

            });




        }
      });
    }
  }

  function prepModelsAndAnimations() {
    //console.log(models)
    Object.values(models).forEach(model => {
      const animsByName = {};
      model.gltf.animations.forEach((clip) => {
        animsByName[clip.name] = clip;
        // Should really fix this in .blend file
        if (clip.name === 'Walk') {
          clip.duration /= 2;
        }
      });
      model.animations = animsByName;
    });
  }

  function playerSphereCollision(sphere) {


    const sphere_center = sphere.collider.center;

    const r = playerCollider.radius + sphere.collider.radius;
    const r2 = r * r;

    //console.log(r2)

    // approximation: player = 3 spheres

    for (const point of [playerCollider.end]) {

      const d2 = point.distanceToSquared(sphere.mesh.position);

      //console.log(spheres)


      if (d2 < r2) {
        // console.log("colode sphire")
        // console.log(d2)
        scene.remove(sphere.mesh)



      }

    }

  }


  function updateSpheres(deltaTime) {

    spheres.forEach(sphere => {

      sphere.collider.center.addScaledVector(sphere.velocity, deltaTime);

      playerSphereCollision(sphere);

    });


  }


  class SkinInstance {
    constructor(model) {
      this.model = model;

      this.skeleton = new THREE.SkeletonHelper(this.model.gltf.scene);
      this.skeleton.visible = true;
      scene.add(this.skeleton);


      //this.animRoot = SkeletonUtils.clone(this.model.gltf.scene);
      //this.model.gltf.scene.add(this.animRoot)
      this.mixer = new THREE.AnimationMixer(this.model.gltf.scene);
      this.actions = {};
    }
    setAnimation(animName) {
      const clip = this.model.animations[animName];
      // turn off all current actions
      for (const action of Object.values(this.actions)) {
        action.enabled = false;
      }
      // get or create existing action for clip
      const action = this.mixer.clipAction(clip);
      action.enabled = true;

      action.setEffectiveTimeScale(1);
      action.setEffectiveWeight(100);

      action.reset();
      action.play();
      this.actions[animName] = action;
      console.log(this.actions)
    }
    update() {

      this.mixer.update(globals.deltaTime);
    }
  }





  const GRAVITY = 30;


  const STEPS_PER_FRAME = 5;

  const globals = {
    time: 0,
    deltaTime: 0,
  };

  const worldOctree = new Octree();

  const playerCollider = new Capsule(new THREE.Vector3(0, 0.35, 0), new THREE.Vector3(0, 1, 0), 0.35);

  const playerVelocity = new THREE.Vector3();
  const playerDirection = new THREE.Vector3();
  let playerOnFloor = false;
  let mouseTime = 0;

  const keyStates = {};

  const vector1 = new THREE.Vector3();
  const vector2 = new THREE.Vector3();
  const vector3 = new THREE.Vector3();

  document.addEventListener('keydown', (event) => {

    keyStates[event.code] = true;

  });

  document.addEventListener('keyup', (event) => {

    keyStates[event.code] = false;

  });



  document.addEventListener('mouseup', () => {

    if (document.pointerLockElement !== null) throwBall();

  });

  document.body.addEventListener('mousemove', (event) => {

    if (document.pointerLockElement === document.body) {

      camera.rotation.y -= event.movementX / 500;
      camera.rotation.x -= event.movementY / 500;

    }

  });

  window.addEventListener('resize', onWindowResize);

  function onWindowResize() {

    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();

    renderer.setSize(window.innerWidth, window.innerHeight);

  }


  function playerCollisions() {

    const result = worldOctree.capsuleIntersect(playerCollider);

    playerOnFloor = false;

    if (result) {




      playerOnFloor = result.normal.y > 0;



      if (!playerOnFloor) {

        console.log("collision")
        playerVelocity.addScaledVector(result.normal, - result.normal.dot(playerVelocity));

      }

      playerCollider.translate(result.normal.multiplyScalar(result.depth));

    }

  }

  function updatePlayer(deltaTime) {


    // console.log(playerCollider.start)
    let damping = Math.exp(- 4 * deltaTime) - 1;

    if (!playerOnFloor) {

      playerVelocity.y -= GRAVITY * deltaTime;

      // small air resistance
      damping *= 0.1;

    }

    playerVelocity.addScaledVector(playerVelocity, damping);

    const deltaPosition = playerVelocity.clone().multiplyScalar(deltaTime);
    //console.log(deltaPosition)
    playerCollider.translate(deltaPosition);

    playerCollisions();

    //camera.position.copy(playerCollider.end);

  }





  function getForwardVector() {

    camera.getWorldDirection(playerDirection);
    playerDirection.y = 0;
    playerDirection.normalize();

    return playerDirection;

  }

  function getSideVector() {

    camera.getWorldDirection(playerDirection);
    //playerDirection.y = 0;
    playerDirection.normalize();
    playerDirection.cross(camera.up);

    return playerDirection;

  }

  function controls(deltaTime) {

    var delta = clock.getDelta(); // seconds.
    var moveDistance = 5 * delta; // 200 pixels per second
    var rotateAngle = Math.PI / 2 * delta;   // pi/2 radians (90 degrees) per second
    // gives a bit of air control
    const speedDelta = deltaTime * (playerOnFloor ? 25 : 8);

    if (keyStates['KeyE']) {

      models.player.gltf.scene.translateZ(-moveDistance);

      //playerVelocity.add(getForwardVector().multiplyScalar(speedDelta));
      //console.log(camera.position)
      // models.player.gltf.scene.position.copy(camera.position)
    }

    if (keyStates['ArrowUp']) {
      models.player.gltf.scene.translateZ(-moveDistance);
    }

    if (keyStates['KeyS']) {

      models.player.gltf.scene.translateZ(moveDistance);
      //playerVelocity.add(getForwardVector().multiplyScalar(- speedDelta));

    }
    if (keyStates['ArrowDown']) {
      models.player.gltf.scene.translateZ(moveDistance);
    }

    if (keyStates['KeyA']) {

      models.player.gltf.scene.translateX(-moveDistance);
      //playerVelocity.add(new THREE.Vector3(0.1, 0.0, -0.1).multiplyScalar(- speedDelta));
      //camera.lookAt(playerDirection)
    }

    if (keyStates['KeyD']) {

      models.player.gltf.scene.translateX(moveDistance);
      //playerVelocity.add(new THREE.Vector3(0.1, 0.0, 0.1).multiplyScalar(speedDelta));

    }

    //console.log(keyStates)

    //rotate left/right/up/down
    var rotation_matrix = new THREE.Matrix4().identity();
    if (keyStates['ArrowLeft']) {
      models.player.gltf.scene.rotateOnAxis(new THREE.Vector3(0, 1, 0), rotateAngle);
    }

    if (keyStates['ArrowRight']) {
      models.player.gltf.scene.rotateOnAxis(new THREE.Vector3(0, 1, 0), -rotateAngle);
    }

    // if (playerOnFloor) {

    //   if (keyStates['Space']) {

    //     playerVelocity.y = 15;

    //   }

    // }

    var relativeCameraOffset = new THREE.Vector3(0, 2, 4);


    if (models.player.gltf != null) {
      //console.log(models.player.gltf.scene.matrixWorld)
      var cameraOffset = relativeCameraOffset.applyMatrix4(models.player.gltf.scene.matrixWorld);
      camera.position.x = cameraOffset.x;
      camera.position.y = cameraOffset.y;
      camera.position.z = cameraOffset.z;

      camera.lookAt(models.player.gltf.scene.position);

      playerCollider.end = models.player.gltf.scene.position;
    }

  }


  function teleportPlayerIfOob() {

    if (camera.position.y <= - 25) {
      playerCollider.start.set(0, 0.35, 0);
      playerCollider.end.set(0, 1, 0);
      playerCollider.radius = 0.35;
      camera.position.copy(playerCollider.end);
      camera.rotation.set(0, 0, 0);

    }

  }

  let skinInstance;

  function init() {
    console.log("init")
    console.log(models.player)
    prepModelsAndAnimations()
    skinInstance = new SkinInstance(models.player);
    skinInstance.setAnimation('Run');
    //console.log(Player);



  }

  let then = 0;
  // Main loop
  const animation = (now) => {

    globals.time = now * 0.001;
    // make sure delta time isn't too big.
    globals.deltaTime = Math.min(globals.time - then, 1 / 20);
    then = globals.time;


    // const deltaTime = Math.min(0.05, clock.getDelta()) / STEPS_PER_FRAME;

    // we look for collisions in substeps to mitigate the risk of
    // an object traversing another too quickly for detection.


    if (skinInstance != null) {

      //console.log("walk");
      skinInstance.update();

    }

    for (let i = 0; i < STEPS_PER_FRAME; i++) {

      controls(globals.deltaTime);

      updatePlayer(globals.deltaTime);

      updateSpheres(globals.deltaTime);

      // if (skinInstance != null) {

      //   //console.log("walk");
      //   skinInstance.update();

      // }
      teleportPlayerIfOob();

    }
    renderer.render(scene, camera);

    requestAnimationFrame(animation);
  }
  // renderer.setAnimationLoop(animation); // requestAnimationFrame() replacement, compatible with XR

  requestAnimationFrame(animation);

};
//init();

main()